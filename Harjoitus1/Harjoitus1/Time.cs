﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harjoitus1
{
    class Time
    {
        int hour, minute, second;

        public Time()
        {
            hour = 1;
            minute = 12;
            second = 34;
        }

        public int getHour()
        {
            return hour;
        }

        public int getMinute()
        {
            return minute;
        }

        public int getSecond()
        {
            return second;
        }

        public void setHour(int hour)
        {
            this.hour = hour;
        }

        public void setMinute(int minute)
        {
            this.minute = minute;
        }

        public void setSecond(int second)
        {
            this.second = second;
        }

        public void setTime(int hour, int minute, int second)
        {
            this.hour = hour;
            this.minute = minute;
            this.second = second;
        }

        public override string ToString()
        {
            return hour + ":" + minute + "" + second;
        }

        public Time nextSecond()
        {
            second += 1;
        }
        public Time previousSecond()
        {
            second -= 1;
        }
    }
}
