﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harjoitus1
{
    class Rectangle
    {
        private float lenght;
        private float width;

        public Rectangle()
        {
            lenght = 1.0f;
            width = 1.0f;
        }

        public Rectangle(float l, float w)
        {
            lenght = l;
            width = w;
        }

        public float getLenght()
        {
            return lenght;
        }

        public void setLenght()
        {
            lenght = 2;
        }

        public float getWidth()
        {
            return width;
        }

        public void setWidth()
        {
            width = 2;
        }

        public double getArea()
        {
            return (width * lenght);
        }

        public double getPerimeter()
        {
            return (2 * width + 2 * lenght);
        }

        public override String ToString()
        {
            return lenght + " " + width;
        }

    }
}
