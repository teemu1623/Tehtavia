﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harjoitus1
{
    class Account
    {
        string id;
        string name;
        int balance = 0;

        public Account()
        {
            id = "31";
            name = "Pauli";
            balance = 300;
        }

        public string getID()
        {
            return id;
        }

        public string getName()
        {
            return name;
        }

        public int getBalance()
        {
            return balance;
        }

        public int credit(int amount)
        {
            balance += amount;
            return balance;
        }

        public int debit(int amount)
        {
            if(amount <= balance)
            {
                balance = balance - amount;
            }
            else
            {
                Console.WriteLine("Amount exceeded balance");
            }
            return balance;
        }

        public int transferTo(int amount, Account another)
        {
            if(amount <= balance)
            {
                balance -= amount;
                another.balance += balance;
            }
            else
            {
                Console.WriteLine("Amount exceeded balance");
            }
            return balance;
        }
        public override String ToString()
        {
            return id + " " + name + " " + balance;
        }
    }
}
