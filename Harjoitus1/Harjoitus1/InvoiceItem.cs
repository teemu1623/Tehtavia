﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harjoitus1
{
    class InvoiceItem
    {
        string id;
        string desc;
        int qty;
        double unitPrice;

        public InvoiceItem()
        {
            id = "Tuoli";
            desc = "Vihreä";
            qty = 3;
            unitPrice = 32.20;
        }

        public string getID()
        {
            return id;
        }

        public string getDesc()
        {
            return desc;
        }

        public int getQty()
        {
            return qty;
        }

        public void setQty()
        {
            qty = 4;
        }

        public double getUnitPrice()
        {
            return unitPrice;
        }

        public void setUnitPrice()
        {
            unitPrice = 35.50;
        }

        public double getTotal()
        {
            return unitPrice * qty;
        }

        public override String ToString()
        {
            return id + " " + desc + " " + qty + " " + unitPrice;
        }
    }
}
