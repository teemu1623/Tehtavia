﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harjoitus1
{
    class Employee
    {
        int id;
        string firstName;
        string lastName;
        int salary;

        public Employee()
        {
            id = 15;
            firstName = "Pasi";
            lastName = "Parkkinen";
            salary = 2000;
        }

        public int getID()
        {
            return id;
        }

        public string getFirstName()
        {
            return firstName;
        }

        public string getLastName()
        {
            return lastName;
        }

        public string getName()
        {
            return firstName + " " + lastName;
        }

        public int getSalary()
        {
            return salary;
        }

        public void setSalary()
        {
            salary = 2200;
        }
        public int getAnnualSalary()
        {
            return salary * 12 * salary;
        }
        public int raiseSalary()
        {
            return salary + salary / 5;
            // Nostaa palkka viidellä prosentilla
        }
        public override String ToString()
        {
            return id + " " + firstName + " " + lastName + " " + salary;
        }
    }
}
